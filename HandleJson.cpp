#include "HandleJson.h"

HandleJson::HandleJson(std::string msg)
{
    body = msg;
    printf("Processing..\n");
    AnalyseJsonMsg();
}

// Split out the Key-Value pairs so they could be used individually
// Note that this assumes root value that enacapsulates a set of key-values
void HandleJson::AnalyseJsonMsg()
{
     std::string res;

     if(reader.parse(body, value))
     {
        printf(" json payload OK \n");

        if(value.isObject())
        {
            for (auto const& id : value.getMemberNames()) 
            {
                printf("processing key-values for member '%s'\n", id.c_str());

                //..For each 'top-'level' Memeber

                Json::Value value_next = value[id] ;

                //.. find the key-values

                for( Json::ValueIterator itr = value_next.begin() ; itr != value_next.end() ; itr++ )
                {            
                    Json::Value key = itr.key();

                    Json::Value val = (*itr);

                    printf("Key: %s \n", key.asCString());

                    if(val.isString()) 
                    {
                        printf( "value -> %s\n", val.asCString() ); 
                    } 
                    else if(val.isBool()) 
                    {
                        printf( "value -> %d\n", val.asBool() ); 
                    } 
                    else if(val.isInt()) 
                    {
                        printf( "value -> %d\n", val.asInt() ); 
                    } 
                    else if(val.isUInt()) 
                    {
                        printf( "value -> %d\n", val.asUInt() ); 
                    } 
                    else if(val.isDouble()) 
                    {
                        printf( "value -> %f\n", val.asDouble()); 
                    }
                    else 
                    {
                        printf( " unknown type=[%d]\n", val.type() ); 
                    }
                }
            }
        }
        else
        {
            printf(" Not a json object \n");
        }
     }
     else
     {
        res = reader.getFormatedErrorMessages();
        printf("error json %s \n", res.c_str());
     }
}


//If the memeber and it's Key(s) are known, use that to get the values
std::string HandleJson::GetJsonValue(std::string root,
                                     std::string jsonkey)
{
    std::string res;

    if(reader.parse(body, value))
    {

        res = value[root].get(jsonkey, "json key-val error").asString();

        printf("got json value %s \n", res.c_str());
    }
    else
    {
        res = reader.getFormatedErrorMessages();
        printf("error json %s \n", res.c_str());
    }

    return res;
}