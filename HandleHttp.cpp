#include "HandleHttp.h"

//Sourced from
//http://www.iana.org/assignments/media-types/media-types.xhtml

const char * HTTP_CONTENT_TYPE_APPL_JSON   = "application/json";
const char * HTTP_CONTENT_TYPE_APPL_XML    = "application/xml";
const char * HTTP_CONTENT_TYPE_TEXT_PLAIN  = "text/plain";
const char * HTTP_CONTENT_TYPE_TEXT_HTML   = "text/html";
const char * HTTP_CONTENT_TYPE_TEXT_XML    = "text/xml";

//callback function
size_t callback(void *ptr, size_t size, size_t nmemb, std::string* data)
{
    data->append((char*) ptr, size * nmemb);
    return size * nmemb;
}

HandleHttp::HandleHttp(std::string arg)
{
         printf("Constructing with string arg \n...");
         url = arg;
         content_type_str = "";
}

void HandleHttp::Execute()
{
    http_server_ok = false;

    CURL *curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

    CURLcode res = curl_easy_perform(curl);

    if(res != CURLE_OK)
    {
       http_server_ok = false;
       printf("curl error %d\n", res);
       return;
    }

    res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

    if(res == CURLE_OK)
    {
        printf("curl response code %d\n", response_code);

        if(response_code  > 199 && response_code  < 300)
        {
            printf("Server response is OK!\n");
            http_server_ok = true;
        }
        else
        {
            printf("Server Problem...\n");
        }
    }

    int content_length;

    res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_UPLOAD, &content_length);

    if(res == CURLE_OK)
    {
        printf("specified size of the upload: %d\n", content_length);
    }

    long header_size;

    res = curl_easy_getinfo(curl, CURLINFO_HEADER_SIZE, &header_size);

    if(res == CURLE_OK)
    {
      printf("size of retrieved headers: %ld bytes\n", header_size);
    }

    double download_size;

    res = curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD, &download_size);

    if(res == CURLE_OK)
    {
      printf("number of downloaded bytes %.0f bytes\n", download_size);
    }

    char *content_type = NULL;

    res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &content_type);

    if(res == CURLE_OK && content_type) {
       printf("Content-Type: %s\n", content_type);

       content_type_str = content_type;
    }

    char *ip;

    res = curl_easy_getinfo(curl, CURLINFO_PRIMARY_IP, &ip);

    if(res == CURLE_OK)
    {
       printf("IP Address: %s\n", ip);
    }

    curl_easy_cleanup(curl);
}

bool HandleHttp::IsJson()
{
    return(IsStringPresent(HTTP_CONTENT_TYPE_APPL_JSON));
}

bool HandleHttp::IsHtml()
{
     return(IsStringPresent(HTTP_CONTENT_TYPE_TEXT_HTML));   
}

bool HandleHttp::IsPlain()
{
     return(IsStringPresent(HTTP_CONTENT_TYPE_TEXT_PLAIN));   
}

//TODO
bool HandleHttp::IsXml()
{
     if(IsStringPresent(HTTP_CONTENT_TYPE_APPL_XML))
     {
        return true;
     }
     else if(IsStringPresent(HTTP_CONTENT_TYPE_TEXT_XML))
     {
         return true;
     }
     else
     {
         return false;
     }
}

bool HandleHttp::IsStringPresent(std::string str)
{
     std::size_t present = content_type_str.find(str);

     if(present!=std::string::npos)
     {
        return true;
     }

    return false;  
}

std::string HandleHttp::RemoveHttpHeaders(std::string rsp)
{
 //TODO
 
}