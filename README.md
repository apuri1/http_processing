# HTTP Processing

Querying HTTP servers...

To build:

`cmake -G "Unix Makefiles"`

`make`

To run:

./HandleHttp `<URL>`

Dependencies:

curl
jsoncpp
poco
