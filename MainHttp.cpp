#include "HandleHttp.h"
#include "HandleJson.h"
#include "HandleXml.h"

int main(int argc, char *arg[])
{
     if(argc < 2)
     {
        printf("Provide a url\n");
        exit(0);
     }

     std::string url = arg[1];
     std::string response;
     printf("Got URL arg: %s\n", url.c_str());
     int input, status_code;

     HandleHttp h_http(url);

     h_http.Execute();

     response = h_http.GetResponse();

     if(h_http.IsJson())
     {
        printf("Json payload in response:\n %s \n", response.c_str());

        HandleJson h_json(response);
     }
     else if(h_http.IsHtml())
     {
        printf("Html response:\n %s \n", response.c_str());
        ///TODO HTML processing
     }
     else if(h_http.IsPlain())
     {
        printf("plain text response:\n %s \n", response.c_str());
     }
     else if(h_http.IsXml())
     {
        printf("xml response:\n %s \n", response.c_str());
        ///TODO XML processing

        HandleXml h_xml(response);


     }
     else
     {
        printf("Other \n %s \n", response.c_str());
     }

     printf("Done\n");
     exit(0);
}