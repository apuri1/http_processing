#ifndef H_HANDLE_JSON
#define H_HANDLE_JSON
#endif

#include "json.h"

class HandleJson
{
   public:
     HandleJson(std::string msg);

     void AnalyseJsonMsg();

     std::string GetJsonValue(std::string root,
                              std::string jsonkey);

    private:
         Json::Reader reader;
         Json::Value value;

         std::string body;
};