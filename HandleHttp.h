#ifndef H_HANDLE_HTTP
#define H_HANDLE_HTTP
#endif

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstring>

#include <curl/curl.h>

class HandleHttp
{
   public:

    HandleHttp(std::string arg);

    void Execute();
    bool IsJson();
    bool IsHtml();
    bool IsPlain();
    bool IsXml();
    int GetResponseCode(){ return response_code; }
    std::string RemoveHttpHeaders(std::string rsp);
    std::string GetResponse(){ return response; }

   private:

    bool IsStringPresent(std::string str);

    std::string url, response, content_type_str, additional_info;
    long http_version;
    bool http_server_ok;
    long response_code;
    int err_code;



};